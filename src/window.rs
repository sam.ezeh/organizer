use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib, CompositeTemplate};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/uk/ezeh/organizer/window.ui")]
    pub struct OrganizerWindow {
        // Template widgets
        #[template_child]
        pub header_bar: TemplateChild<gtk::HeaderBar>,
        #[template_child]
        pub label: TemplateChild<gtk::Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for OrganizerWindow {
        const NAME: &'static str = "OrganizerWindow";
        type Type = super::OrganizerWindow;
        type ParentType = gtk::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for OrganizerWindow {}
    impl WidgetImpl for OrganizerWindow {}
    impl WindowImpl for OrganizerWindow {}
    impl ApplicationWindowImpl for OrganizerWindow {}
}

glib::wrapper! {
    pub struct OrganizerWindow(ObjectSubclass<imp::OrganizerWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl OrganizerWindow {
    pub fn new<P: glib::IsA<gtk::Application>>(application: &P) -> Self {
        glib::Object::new(&[("application", application)])
            .expect("Failed to create OrganizerWindow")
    }
}
